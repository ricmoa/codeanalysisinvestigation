﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryingCAconfigDotNFW461
{
    // Violates CA1200
    /// <summary>
    /// Type <see cref="T:C" /> contains method <see cref="F:C.F" />
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world");
        }

        // This method violates the rule.
        public static string GetSomething(int first, int second)
        {
            return first.ToString();
        }
    }
}
